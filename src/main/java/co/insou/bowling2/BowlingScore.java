package co.insou.bowling2;

import co.insou.bowling2.panel.Frame;
import co.insou.bowling2.panel.Panel;

public abstract class BowlingScore {

    Frame frame;
    Panel panel;

    final int value1;
    final int value2;

    BowlingScore(int value1, int value2) {
        this.value1 = value1;
        this.value2 = value2;
    }

    public abstract int getCalculatedScore();

    public int getScorePoints() {
        return value1 + value2;
    }

    public int getValue1() {
        return value1;
    }

    public int getValue2() {
        return value2;
    }

    public void setFrame(Frame frame) {
        this.frame = frame;
    }

    public void setPanel(Panel panel) {
        this.panel = panel;
    }

}

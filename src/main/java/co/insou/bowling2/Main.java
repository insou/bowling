package co.insou.bowling2;

import co.insou.bowling2.panel.Panel;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class Main {

    public static void main(String[] args) {
        new Main();
    }

    private Main() {
        System.out.println("Please enter your input: ");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        Panel panel = new Panel(parseInput(input));
        System.out.println("Score: " + panel.calculateScore());
    }

    private List<Integer> parseInput(String input) {
        List<Integer> values = new ArrayList<>();
        for (String str : input.split("\\s")) {
            if (str.isEmpty()) {
                continue;
            }
            try {
                values.add(Integer.parseInt(str));
            } catch (NumberFormatException e) {
                System.err.println("Bad input!");
                System.exit(1);
                return null;
            }
        }
        return values;
    }

}

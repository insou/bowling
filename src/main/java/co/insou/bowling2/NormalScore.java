package co.insou.bowling2;

public class NormalScore extends BowlingScore {

    public NormalScore(int value1, int value2) {
        super(value1, value2);
    }

    @Override
    public int getCalculatedScore() {
        if (this.frame == null || this.panel == null) {
            throw new IllegalStateException("frame == null: " + (frame == null) + ", panel == null: " + (panel == null));
        }
        return this.value1 + this.value2;
    }

}

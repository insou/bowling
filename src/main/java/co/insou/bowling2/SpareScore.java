package co.insou.bowling2;

public class SpareScore extends BowlingScore {

    public SpareScore(int value1) {
        super(value1, 10 - value1);
    }

    @Override
    public int getCalculatedScore() {
        if (this.frame == null || this.panel == null) {
            throw new IllegalStateException("frame == null: " + (frame == null) + ", panel == null: " + (panel == null));
        }
        int frameNumber = this.frame.getFrameNumber();
        if (frameNumber >= 9) {
            return 10;
        }
        if (this.panel.frameDoesNotExist(frameNumber + 1)) {
            return this.getScorePoints();
        }
        return this.getScorePoints() + this.panel.getFrame(frameNumber + 1).getFirstValue();
    }

}

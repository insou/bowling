package co.insou.bowling2;

import co.insou.bowling2.panel.Frame;

public class StrikeScore extends BowlingScore {

    public StrikeScore() {
        super(10, 0);
    }

    @Override
    public int getCalculatedScore() {
        if (this.frame == null || this.panel == null) {
            throw new IllegalStateException("frame == null: " + (frame == null) + ", panel == null: " + (panel == null));
        }
        int frameNumber = this.frame.getFrameNumber();
        if (frameNumber >= 9) {
            return 10;
        }
        if (this.panel.frameDoesNotExist(frameNumber + 1)) {
            return 10;
        }
        Frame nextFrame = this.panel.getFrame(frameNumber + 1);
        if (nextFrame.isStrike()) {
            if (this.panel.frameDoesNotExist(frameNumber + 2)) {
                return 20;
            }
            return 20 + this.panel.getFrame(frameNumber + 2).getFirstValue();
        }
        return 10 + nextFrame.getScorePoints();
    }

}

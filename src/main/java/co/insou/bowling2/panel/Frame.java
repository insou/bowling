package co.insou.bowling2.panel;

import co.insou.bowling2.BowlingScore;
import co.insou.bowling2.NormalScore;
import co.insou.bowling2.SpareScore;
import co.insou.bowling2.StrikeScore;

public class Frame {

    private final int frameNumber;
    private final BowlingScore score;

    public Frame(int frameNumber, BowlingScore score) {
        this.frameNumber = frameNumber;
        this.score = score;
        this.score.setFrame(this);
    }

    public void setPanel(Panel panel) {
        this.score.setPanel(panel);
    }

    public int getScorePoints() {
        return this.score.getScorePoints();
    }

    public int getCalculatedScore() {
        return this.score.getCalculatedScore();
    }

    public int getFirstValue() {
        return this.score.getValue1();
    }

    public int getSecondValue() {
        return this.score.getValue2();
    }

    public boolean isStrike() {
        return this.score instanceof StrikeScore;
    }

    public boolean isSpare() {
        return this.score instanceof SpareScore;
    }

    public boolean isNormal() {
        return this.score instanceof NormalScore;
    }

    public int getFrameNumber() {
        return frameNumber;
    }
}

package co.insou.bowling2.panel;

import co.insou.bowling2.BowlingScore;
import co.insou.bowling2.NormalScore;
import co.insou.bowling2.SpareScore;
import co.insou.bowling2.StrikeScore;

import java.util.ArrayList;
import java.util.List;

public class Panel {

    private final List<Frame> frames = new ArrayList<>();

    public Panel(List<Integer> bowls) {
        addBowls(bowls);
        frames.forEach(frame -> frame.setPanel(this));
    }

    public int calculateScore() {
        // final one element array to allow lambda manipulation
        final int[] score = {0};
        frames.forEach(frame -> score[0] += frame.getCalculatedScore());
        return score[0];
    }

    private void addBowls(List<Integer> bowls) {
        while (bowls.size() > 0) {
            if (frames.size() == 12) {
                break;
            }
            if (bowls.get(0) == 10) {
                addBowl(new StrikeScore());
                bowls.remove(0);
            } else if (bowls.size() > 1) {
                if (bowls.get(0) + bowls.get(1) == 10) {
                    addBowl(new SpareScore(bowls.get(0)));
                    bowls.remove(0);
                    bowls.remove(0);
                } else {
                    addBowl(new NormalScore(bowls.get(0), bowls.get(1)));
                    bowls.remove(0);
                    bowls.remove(0);
                }
            } else {
                break;
            }
        }
    }

    private void addBowl(BowlingScore score) {
        frames.add(new Frame(frames.size(), score));
    }

    public boolean frameDoesNotExist(int index) {
        return frames.size() <= index;
    }

    public Frame getFrame(int index) {
        return frames.get(index);
    }

}

package co.insou.bowling2;

import co.insou.bowling2.panel.Frame;
import co.insou.bowling2.panel.Panel;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class NormalTest {

    private Frame normalFrame1;
    private Frame normalFrame2;

    @Before
    public void setUp() throws Exception {
        Panel panel = new Panel(new ArrayList<>(Arrays.asList(4, 0, 10, 10, 6, 4, 10, 5, 5, 7, 2, 8, 1, 10, 10, 10, 10)));
        normalFrame1 = panel.getFrame(0);
        normalFrame2 = panel.getFrame(7);
    }

    @Test
    public void testGetCalculatedScore() throws Exception {
        assertEquals(4, normalFrame1.getFirstValue());
        assertEquals(0, normalFrame1.getSecondValue());
        assertEquals(4, normalFrame1.getScorePoints());
        assertEquals(4, normalFrame1.getCalculatedScore());

        assertEquals(8, normalFrame2.getFirstValue());
        assertEquals(1, normalFrame2.getSecondValue());
        assertEquals(9, normalFrame2.getScorePoints());
        assertEquals(9, normalFrame2.getCalculatedScore());
    }

}

package co.insou.bowling2;

import co.insou.bowling2.panel.Panel;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class PanelTest {

    private Panel panel;

    @Before
    public void setUp() throws Exception {
        panel = new Panel(new ArrayList<>(Arrays.asList(4, 0, 10, 10, 6, 4, 10, 5, 5, 7, 2, 8, 1, 10, 10, 10, 10)));
    }

    @Test
    public void testGetCalculatedScore() throws Exception {
        assertEquals(185, panel.calculateScore());
    }
}
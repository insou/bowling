package co.insou.bowling2;

import co.insou.bowling2.panel.Frame;
import co.insou.bowling2.panel.Panel;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;


import static org.junit.Assert.*;

public class FrameTest {

    private Frame normalFrame;
    private Frame spareFrame;
    private Frame strikeFrame;

    @Before
    public void setUp() throws Exception {
        Panel panel = new Panel(new ArrayList<>(Arrays.asList(4, 0, 10, 10, 6, 4, 10, 5, 5, 7, 2, 8, 1, 10, 10, 10, 10)));
        normalFrame = panel.getFrame(7);
        spareFrame = panel.getFrame(3);
        strikeFrame = panel.getFrame(8);
    }

    @Test
    public void testGetCalculatedScore() throws Exception {
        assertEquals(true, normalFrame.isNormal());
        assertEquals(false, normalFrame.isSpare());
        assertEquals(false, normalFrame.isStrike());

        assertEquals(false, spareFrame.isNormal());
        assertEquals(true, spareFrame.isSpare());
        assertEquals(false, spareFrame.isStrike());

        assertEquals(false, strikeFrame.isNormal());
        assertEquals(false, strikeFrame.isSpare());
        assertEquals(true, strikeFrame.isStrike());
    }
}
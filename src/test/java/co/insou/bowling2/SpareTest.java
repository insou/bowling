package co.insou.bowling2;

import co.insou.bowling2.panel.Frame;
import co.insou.bowling2.panel.Panel;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class SpareTest {

    private Frame spareFrame1;
    private Frame spareFrame2;

    @Before
    public void setUp() throws Exception {
        Panel panel = new Panel(new ArrayList<>(Arrays.asList(4, 0, 10, 10, 6, 4, 10, 5, 5, 7, 2, 8, 1, 10, 10, 10, 10)));
        spareFrame1 = panel.getFrame(3);
        spareFrame2 = panel.getFrame(5);
    }

    @Test
    public void testGetCalculatedScore() throws Exception {
        assertEquals(6, spareFrame1.getFirstValue());
        assertEquals(4, spareFrame1.getSecondValue());
        assertEquals(10, spareFrame1.getScorePoints());
        assertEquals(20, spareFrame1.getCalculatedScore());

        assertEquals(5, spareFrame2.getFirstValue());
        assertEquals(5, spareFrame2.getSecondValue());
        assertEquals(10, spareFrame2.getScorePoints());
        assertEquals(17, spareFrame2.getCalculatedScore());
    }

}

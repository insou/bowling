package co.insou.bowling2;

import co.insou.bowling2.panel.Frame;
import co.insou.bowling2.panel.Panel;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class StrikeTest {

    private Frame strikeFrame1;
    private Frame strikeFrame2;

    @Before
    public void setUp() throws Exception {
        Panel panel = new Panel(new ArrayList<>(Arrays.asList(4, 0, 10, 10, 6, 4, 10, 5, 5, 7, 2, 8, 1, 10, 10, 10, 10)));
        strikeFrame1 = panel.getFrame(1);
        strikeFrame2 = panel.getFrame(8);
    }

    @Test
    public void testGetCalculatedScore() throws Exception {
        assertEquals(10, strikeFrame1.getFirstValue());
        assertEquals(10, strikeFrame1.getScorePoints());
        assertEquals(26, strikeFrame1.getCalculatedScore());

        assertEquals(10, strikeFrame2.getFirstValue());
        assertEquals(10, strikeFrame2.getScorePoints());
        assertEquals(30, strikeFrame2.getCalculatedScore());
    }

}
